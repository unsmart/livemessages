/*
 * LiveMessages: Let streamer's globally broadcast their stream!
 */
package mysticgaming.unsmart.LiveMessages;


import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * @author Unsmart from play.mysticgaming.us
 */

public class Main extends Plugin {
    static String messageYT, messageTwitch, messageMixer, mixerNotLive, twitchNotLive;
    private boolean error = false;

    @Override
    public void onEnable() {
        try {
            getProxy().getPluginManager().registerCommand(this, new live());
        } catch (Error e) {
            getLogger().severe("Plugin couldn't load commands.");
            error = true;
        }
        if (!getDataFolder().exists())
            //noinspection ResultOfMethodCallIgnored
            getDataFolder().mkdir();

        File file = new File(getDataFolder(), "config.yml");


        if (!file.exists()) {
            try (InputStream in = getResourceAsStream("config.yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            this.getConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!error)
            getLogger().info("Has successfully been enabled with no errors!");
        else{
            getLogger().severe("Has not loaded properly and will not work! Please report any errors in console to the dev on Spigot!");
            this.onDisable();
        }


    }

    public void onDisable(){
        getLogger().info("Has been disabled thanks for using my plugin! ~ Unsmart (dev)");
    }

    private void getConfig() throws IOException {
        Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
        messageYT = config.getString("messageYT").replaceAll("(&([a-f0-9]))", "\u00A7$2");
        messageTwitch = config.getString("messageTwitch").replaceAll("(&([a-f0-9]))", "\u00A7$2");
        messageMixer = config.getString("messageMixer").replaceAll("(&([a-f0-9]))", "\u00A7$2");
        mixerNotLive = config.getString("messageMixerNotLive").replaceAll("(&([a-f0-9]))", "\u00A7$2");
        twitchNotLive = config.getString("messageTwitchNotLive").replaceAll("(&([a-f0-9]))", "\u00A7$2");
    }
}
