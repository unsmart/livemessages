package mysticgaming.unsmart.LiveMessages;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


class live extends Command {

    public live() {
        super("live", "lm.live");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {
        String messageYT = Main.messageYT;
        String messageTwitch = Main.messageTwitch;
        String messageMixer = Main.messageMixer;
        String mixerNotLive = Main.mixerNotLive;
        String twitchNotLive = Main.twitchNotLive;
        String getID = "https://www.youtube.com/account_advanced";
        String help = "Yt: \n/live youtube <Channel ID> (found at " + getID + ")" +"\nTwitch:\n/live twitch <Username>\nMixer:\n/live mixer <Username>";

        if ((sender instanceof ProxiedPlayer) && args.length == 0) {
            sender.sendMessage(help);
        }else if ((sender instanceof ProxiedPlayer)) {
            String link;
            String name = sender.getName();

            messageYT = messageYT.replaceAll("%name%", name);
            switch (args[0]) {
                case "yt":
                case "youtube":
                    if (args.length == 1) {
                        sender.sendMessage("Correct usage for youtube: /live yt <Channel ID> (found at " + getID + ")");
                    }
                    try {
                        String sURL = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + args[1] + "&eventType=live&type=video&key=AIzaSyDCjZTGGAKM7bUHAAnR9Ca_YTbLLh9fEiM";
                        JSONObject json = JSONParser.readJsonFromUrl(sURL);
                        JSONObject res = (JSONObject) json.get("pageInfo");
                        int results;
                        results = res.getInt("totalResults");
                        if (results == 0) {
                            sender.sendMessage("Not live?");
                        } else {
                            JSONArray id = json.getJSONArray("items");
                            JSONObject d = id.getJSONObject(0);
                            JSONObject dd = (JSONObject) d.get("id");
                            link = "https://youtube.com/watch?v=" + dd.getString("videoId");
                            messageYT = messageYT.replaceAll("%link%", link);
                            for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
                                p.sendMessage(messageYT);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        sender.sendMessage("ERROR: Channel ID is incorrect please get your channel id from " + getID);
                    }

                    break;
                case "mixer": {

                    if (args.length == 1) {
                        sender.sendMessage("Correct usage for mixer streams: /live mixer <Username>");
                        return;
                    }
                    String channel = args[1];
                    try {
                        String sURL = "http://mixer.api.scorpstuff.com/live.php?user=" + channel;
                        URL u = new URL(sURL);
                        URLConnection c = u.openConnection();
                        InputStream r = c.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(r));
                        messageMixer = messageMixer.replaceAll("%link%", "https://mixer.com/" + channel);
                        messageMixer = messageMixer.replaceAll("%name%", name);

                        if (reader.readLine().equals("LIVE")) {
                            for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
                                p.sendMessage(messageMixer);
                            }
                        } else {
                            sender.sendMessage(mixerNotLive);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
                }
                case "twitch": {
                    if (args.length == 1) {
                        sender.sendMessage("Correct usage for twitch streams: /live twitch <Username>");
                        return;
                    }
                    String channel = args[1];
                    try {
                        String sURL = "http://twitch.api.scorpstuff.com/live.php?user=" + channel;
                        URL u = new URL(sURL);
                        URLConnection c = u.openConnection();
                        InputStream r = c.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(r));
                        messageTwitch = messageTwitch.replaceAll("%link%", "https://twitch.tv/" + channel);
                        messageTwitch = messageTwitch.replaceAll("%name%", name);

                        if (reader.readLine().equals("LIVE")) {
                            for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
                                p.sendMessage(messageTwitch);
                            }
                        } else {
                            sender.sendMessage(twitchNotLive);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                default:
                    sender.sendMessage(help);
                    break;
            }

        } else {
            sender.sendMessage(new TextComponent("Error not a player?"));
        }
    }

}
